package app.com.study.stickyny.retirecalc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    TextView mResultText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText ageEdit = (EditText) findViewById(R.id.age_edit);
        final EditText retireEdit = (EditText) findViewById(R.id.retire_edit);
        mResultText = (TextView) findViewById(R.id.result_text);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int inputAge;
                int inputRetire;
                if (!ageEdit.getText().toString().isEmpty()) {
                    inputAge = Integer.parseInt(ageEdit.getText().toString().trim());

                    if (!retireEdit.getText().toString().isEmpty()) {
                        inputRetire = Integer.parseInt(retireEdit.getText().toString().trim());

                        if (inputAge !=0 && inputRetire !=0) {
                            print(inputAge, inputRetire);
                        }
                    }
                }
            }
        };
        ageEdit.addTextChangedListener(textWatcher);
        retireEdit.addTextChangedListener(textWatcher);
    }

    private void print(int age, int retireAge) {
        int restYearNum = restYear(age, retireAge);
        mResultText.setText(getString(R.string.result_text, restYearNum, thisYear(), retireYear(restYearNum)));
    }
    private int restYear(int age, int retireAge) {
        return retireAge-age;
    }
    private int thisYear() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.YEAR);
    }
    private int retireYear(int restYearNum) {
        return thisYear() + restYearNum;
    }
}
